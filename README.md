# Help
```
git help
```

# Config git
-System
```
git config --system
```

-User
```
git config --global
git config --global user.name "Tien Nguyen"
git config --global user.email "tiennm@dgroup.co"
git config --global core.editor "vim"
git config --global color.ui "true"
```

-Project
```
git config --local
```

# Basic
-Initialize a repository
```
git init
```
-Check status
``` 
git status
```
-Commit changes to the repository with a message
```
git commit -m "<message>"
```

# Making Changes to Files
-Add the change to staging directory

Add all files
```
git add .
git add -A
```

Add a specific file
```
git add <file>
```

-See the change

See all the changes
``` 
git diff
```

See one file changes
```
git diff <file>
```
-View only staged changes
``` 
git diff --staged
```

-Delete file
``` 
git rm <file>
```

-Move and rename files
``` 
git mv <old-name> <new-name> 
```

# Undo Changes
-Undo file changes
``` 
git checkout -- <file>
```
-Unstage files
``` 
git reset HEAD <file>
```
-Amending commits (modify the last commit)
``` 
git commit --amend -m '<file>'
```
-Retrieve old versions
``` 
git checkout <commit> -- <file>
```
-Revert a commit
``` 
git revert <commit>
```

-Use reset to undo commits

--soft: does not change staging index or working directory
``` 
git reset --soft <commit>
```

--mixed (default): changes staging index to match repository, does not change working directory
``` 
git reset --mixed <commit>
```

--hard: changes staging index and working directory to match repository
``` 
git reset --hard <commit>
```

-Remove untracked files
Remove all files, not folders
``` 
git clean -n
```
Remove all files and folders
``` 
git clean -f
```

# Ignoring files

-Use .gitignore files

-What to ignore
compiled source code  
packages and compressed files  
logs and databases  
operating system generated files
user-uploaded assets (images, PDFs, videos)  

-Globally ignore files
``` 
git config --global core.excludesfile ~/.gitignore_global
```

-Ignore tracked files
``` 
git rm --cached <file>
```

-Track empty directories
Add .gitkeep file in empty directories

# Navigating the Commit Tree

-Reference commits
  - parent commit
  -HEAD^, acf857ru^, master^
  -HEAD~1, HEAD~
  
  - grandparent commit
  -HEAD^^, acf857ru^^, master^^
  -HEAD~2

-View the commit log
```
git log
```

-Log n commits
``` 
git log -<n>
```

Log oneline
``` 
git log --oneline
```

Log since a date, until a date
``` 
git log --since=2017-07-01
git log --until=2017-07-01
git log --since="2 weeks ago" --until="3 days ago"
```
Log with author
``` 
git log --author
```

Log with regEx
``` 
git log --grep=<regEx>
```

Log with range of commits
``` 
git log <commit1>..<commit2>
```

Log the commit with changes
``` 
git log -p <commit>..<commit2> <file>
```

Log graph
``` 
git log --graph
```

-View commits

``` 
git show <commit>
```

-Compare commits
``` 
git diff <commit>
git diff <commit> <file>
git diff <commit1>..<commit2>
```

# Branching

-List all branches
``` 
git branch
```
-Create a new branch
``` 
git branch <new_branch>
```

-Switch to a branch
``` 
git checkout -b <branch>
```

-Create and checkout a branch at the same time
``` 
git checkout -b <new_branch>
```

-Compare branches
``` 
git diff <branch1>..<branch2>
git diff --color-words <branch1>..<branch2>
```
-Rename a branch
``` 
git branch -m <old_branch> <new_branch>
```
-Delete a branch
``` 
git branch -d <branch>
git branch -D <branch> (ignore merged)
```

# Merging branches

-Merge code

Fast forward
``` 
git checkout master
git merge <branch>
```
Don't do a fast forward
``` 
git merge --no-ff <branch>
```
Only can do a fast forward
``` 
git merge --ff-only <branch>
```

-True merge
Merge and resolve conflicts (if have)
Abort the merge
``` 
git merge --abort
```

Or manually resolve the merge, then commit

# Stashing Changes

-Save changes in the stash
``` 
git stash save "<message>"
```

-View stashes
``` 
git stash list
```

-View a stash
``` 
git stash show <stash_name>
```

-Retrieve stashed changes
Pull stash back to working directory and remove from stash
``` 
git stash pop <stash_name>
```
Pull stash back to working directory but don't remove from stash
``` 
git stash apply <stash_name>
```
-Delete item from stash
Detele a stash item
``` 
git stash drop <stash_name>
```

Delete all stash
``` 
git stash clear
```

# Remotes

-List all remote repositories
``` 
git remote
```

-Add a remote
```
git remote add <alias> <remote_url>
```

-Remove a remote
``` 
git remote rm <alias>
```

-Clone a remote repository
``` 
git clone <remote_url> <folder_name>
```

-Push to remote
``` 
git push <alias> <branch>
```

-Fetch changes from a remote repository
``` 
git fetch
```

-Fetch and merge
``` 
git pull
```
-Delete a remote branch
``` 
git push origin :<branch>
git push origin --delete <branch>
```